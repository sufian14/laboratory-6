public class task3 {

	public static void main(String[] args) {
		int[] arr1 = { 5, 1, 3, 7, 2, 4 };
		int n = arr1.length;
		int sum = 5;

		System.out.println("Pairs whose sum is " + sum);
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (arr1[i] + arr1[j] == sum)
					System.out.println(arr1[i] + " " + arr1[j]);
			}
		}
	}

}
